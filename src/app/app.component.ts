import { Component, OnInit, ViewChild } from '@angular/core';
import { App, Nav, Platform, NavController, MenuController,LoadingController, ToastController } from 'ionic-angular';

//import { StatusBar, Splashscreen } from 'ionic-native';

import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { ContactPage } from '../pages/contact/contact';
import { QuizPage } from '../pages/quiz/quiz';
import { QuizPage2 } from '../pages/quiz2/quiz2';
import { forgotPasswordPage } from '../pages/lupapassword/lupapassword';
import { BlogPage } from '../pages/blog/blog';
import { DirectoryPage } from '../pages/directory/directory';
import { InstructorsPage } from '../pages/instructors/instructors';
import { ConfigService } from '../services/config';
import { Storage } from '@ionic/storage';
import { ImgcacheService } from '../services/imageCache';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {

  styles:any;
  tabsPage = TabsPage;
  pages:any[]=[];

  rootPage: any = 'HomePage';
  loader: any;

  pressed: boolean = false;
  
  @ViewChild('nav') nav:Nav;
  
    constructor(private config:ConfigService,
        private platform: Platform, 
        private menuCtrl: MenuController,
        private loadingCtrl:LoadingController,
        private app:App,
        private storage:Storage,
        private imgcacheService:ImgcacheService,
        public splashScreen: SplashScreen,
		private toastCtrl: ToastController,) {


        this.presentLoading();

        platform.ready().then(() => {
            if(this.config.settings.rtl){
               platform.setDir('rtl', true);
            }
            this.splashScreen.hide();
            imgcacheService.initImgCache().subscribe(() => {
			this.rootPage = TabsPage;
			this.loader.dismiss();
            });
        });

        //Tracker
        
        this.pages =[
          { title: config.get_translation('home_menu_title'), component: TabsPage, index: 0, hide:false},
          { title: config.get_translation('directory_menu_title'), component: DirectoryPage, index: 2, hide:false},
          { title: config.get_translation('blog_menu_title'), component: BlogPage, index: 1, hide:false},
          { title: config.get_translation('contact_menu_title'), component: ContactPage, index: 3, hide:false},
		  //{ title: config.get_translation('quiz_menu_title'), component: QuizPage, index: 4, hide:false},
        ];

		platform.registerBackButtonAction(() => {
			let nav = this.app.getActiveNavs()[0];
			let activeView = nav.getActive();
			//console.log(activeView.name);
			if (activeView.name === 'HomePage') {
				if (this.pressed)
				{
					this.platform.exitApp();
				}
				if (!this.pressed) { this.pressed=true; console.log('1 Kali');
					let toast = this.toastCtrl.create({
						message: 'Tekan sekali lagi untuk keluar',
						duration: 2000,
						position: 'bottom'
					});
					let x = setTimeout(() =>{
						this.pressed=false;
					},2000);
					toast.present();
				}
			}
			else {
				if (nav.canGoBack()){
					nav.pop();
				} else {
					//nav.goToRoot();
				}
			}
		});
	}

    ngOnInit(){
        this.config.initialize();
    }

    presentLoading() {
        this.loader = this.loadingCtrl.create({
            //content: "Loading..."
        });
        this.loader.present();
    }

    onLoad(page: any){
        let nav = this.app.getRootNav();

        nav.setRoot(page.component,{index:page.index});
        //nav.push(page);
        //this.app.getRootNav().push(page);
        //this.nav.push(page);
        //this.nav.setRoot(page);
        this.menuCtrl.close();
    }
}
