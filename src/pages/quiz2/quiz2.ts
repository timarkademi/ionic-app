import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-quiz2',
  templateUrl: 'quiz2.html'
})

export class QuizPage2{
	
		
	constructor(
		public navCtrl: NavController,
  		private navParams: NavParams,
		public modalCtrl:ModalController,
		private loadingCtrl: LoadingController,	
	){}
	
	
	
	back(){
	
		this.navCtrl.push(HomePage);
	}	
}

