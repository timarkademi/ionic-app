import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController, AlertController } from 'ionic-angular';

import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';


//import { ProfilePage } from '../profile/profile';

import { User } from '../../models/user';
import { Course } from '../../models/course';

import { ConfigService } from '../../services/config';
import { CourseService } from '../../services/course';
import { UserService } from '../../services/users';
import { WishlistService } from '../../services/wishlist';

import { FullCourse } from '../../models/course';

//import { Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import * as WC from 'woocommerce-api';

//import { TransPage } from '../trans/trans';
//import * as midtransClient from 'midtrans-client';
import { OrderPage } from '../order/order';

import { SearchPage } from '../search/search';
import { CoursePage } from '../course/course';
import { ProfilePage } from '../profile/profile';

 
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html'
})

export class CheckoutPage implements OnInit{
	
	isLoggedIn: boolean = false;
	user: User;
	course: Course;
	
	WooCommerce: any;
	// private midTrans: any;
	private user_id: number;
	private baseUrl: string;
	
	userfirst: string;
	userlast: string;
	useremail: string;
	userphone: string;
	usercoupon: string;
	
	useCoupon: boolean = false;
	couponItem: any;
	couponDisc: number=0;
	
	product: any;
	customer: any;
	random_number: number;
	
	//private observable:Observable<any>;
	private http: Http;
	//MTUrl:'https://api.sandbox.midtrans.com';
	
	constructor(
        private config: ConfigService,
  		private navParams: NavParams,
		private userService: UserService,
		public modalCtrl:ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
				  		  
		public navCtrl: NavController,

		) {
		
		this.baseUrl = this.config.settings.url;

		this.WooCommerce = WC({
		url: this.baseUrl,
		//consumerKey: "ck_52f450964c8515c0bd2d7a83fde2ac820ee51384",
		//consumerSecret: "cs_ecdfa2006cc03f8cb571317009f5f212766d85ec",
		consumerKey: "ck_f07aa7a4a60e6b80ac846bbbc16b073f02e50e76",
		consumerSecret: "cs_4f0316d0a5ac11b7788670a0a8ce1a53ab3bf85f",
		wpAPI: true, // Enable the WP REST API integration
		queryStringAuth: true,
		verifySsl: true,
		version: 'wc/v3' // WooCommerce WP REST API version
		});
				
	}

	ngOnInit() {
		// Nyoba bikin halaman baru nyambung ama WooCommerce
		let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,

        });
		loading.present();
		this.course = this.navParams.data['course'];
		this.user=this.config.user;
		let id_confirmed:string;
		let get_id:any =this.course.price_html[0];
		let pos1 = get_id.link.search('=');
		let pos2 = get_id.link.length;
		if (pos1 >=0) {
			id_confirmed = get_id.link.substr(pos1+1,pos2-pos1);
			console.log(id_confirmed);
		}
		//Load Product
		this.WooCommerce.getAsync('products/'+id_confirmed).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				this.product = res;
				this.random_number = (this.getRandomNumber());
				//this.random_number = (this.getRandomNumber() -this.getDiscount(10));
				loading.dismiss();
			} else {
				console.log('bendanya gak ada');
				loading.dismiss();
			}
		});
		// Load Customer
		this.WooCommerce.getAsync('customers/'+this.config.user.id).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				this.customer = res;
				this.userfirst= this.customer.first_name;
				this.userlast= this.customer.last_name;
				this.useremail= this.customer.email;
			} else {
				console.log('orangnya gak ada');
			}
		});
		//this.random_number = this.getRandomNumber();
	}

    openSearch(){
        let modal = this.modalCtrl.create(SearchPage);
        modal.present();
    }
    backToCourse(){
      //this.navCtrl.push(CoursePage,{'id':this.coursestatus.course_id});
    } 
	
	showAlert(message:string=null,message2:string=null) {
		let alert = this.alertCtrl.create({
			title:message,
			subTitle:message2,
			buttons:['Ok']
		});
		alert.present();
	}
	getRandomNumber() {
		return Math.floor(-((Math.random()*899)+100));
	}
	getDiscount(percentage) {
		return Math.round(this.product.price*(percentage/100));
	}
	
	getTotalPrice() {
		return Math.floor(parseInt(this.course.price) + this.random_number + this.couponDisc);
	}
	testOrderItem(){
		let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,

        });
		loading.present();
		
		this.WooCommerce.getAsync('orders/18187').then((data) => {
				let res = (JSON.parse(data.body));
				if (res) {
					for (let x in res)
					{
						console.log(x+':'+res[x]);
					}
					//console.log(JSON.stringify(res));
					console.log('saved');
					this.navCtrl.push(OrderPage,{'order': res});
					//let modal = this.modalCtrl.create(OrderPage,{'order': res});
					//modal.present();
					//loading.dismiss();

					//Ke halaman berikutnya
				} else {
					//return false
					console.log('Kenapa error ya?');
				}
			});
			console.log('Selesai');
	}
	orderItem(){
		if (this.userfirst && this.userlast && this.useremail && this.userphone) {
			// Buat template order
			let item = {
				customer_id:this.user.id,
				status:"on-hold",
				billing:{
					first_name:this.userfirst,
					last_name:this.userlast,
					email:this.useremail,
					phone:this.userphone,
				},
				payment_method:'bacs',
				payment_method_title:"Transfer ke BCA",
				line_items: [
					{
						name:this.product.name,
						product_id:this.product.id,
						quantity:1,
						subtotal:this.product.price,
					}
				],
				fee_lines: [
					{
						name:"Diskon",
						tax_status:"taxable",
						amount:this.random_number.toString(),
						total:this.random_number.toString(),
					}
				],
				coupon_lines:[],
			};
			console.log('Angka Random :'+this.random_number);
			if(this.useCoupon) {
				item.coupon_lines.push(this.couponItem);
			}

			//Masukin ke WooCommerce
			this.WooCommerce.postAsync('orders',item).then((data) => {
				let res = (JSON.parse(data.body));
				if (res) {
					for (let x in res)
					{
						console.log(x+':'+res[x]);
					}
					console.log(JSON.stringify(res));
					console.log('saved');
					this.navCtrl.push(OrderPage,{'order': res});
					//let modal = this.modalCtrl.create(OrderPage,{'order': res});
					//modal.present();
					//Ke halaman berikutnya
				} else {
					//return false
					console.log('Kenapa error ya?');
				}
			});
			console.log('Selesai');
		} else {
			this.showAlert('Data','Isi data anda dengan lengkap');
		}
	}
	
	// is the user already have the same order?
	checkOrders(user_id) {
		console.log("ID:"+user_id);
		this.WooCommerce.getAsync('orders?customer='+user_id).then((data) => {
			let res = (JSON.parse(data.body));
			if (res) {
				for (let x in res)
				{
					console.log(x+':'+res[x]);
				}
				console.log('Ada');
				return true
			} else {
				console.log('Gak ada');
				return false
			}
		});
	}
	removeCoupon(){
		this.couponItem = null;
		this.couponDisc = 0;
		this.useCoupon = false;
	}
	checkCoupon(kupon){
		if (kupon) {
			let loading = this.loadingCtrl.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,
			});
			loading.present();			
		this.WooCommerce.getAsync('coupons?code='+kupon).then((data) => {
			let res = (JSON.parse(data.body));
			if (res.length) {
				for (let x in res)
				{
					console.log(x+':'+res[x]);
				}
				
				if (res[0].discount_type = "percent") {
					this.couponDisc = (((res[0].amount/100) * this.course.price));
				} else {
					this.couponDisc = res[0].amount;
				}
				this.couponItem =
					{
						code:res[0].code,
						discount:this.couponDisc.toString(),
					};
				this.couponDisc = -this.couponDisc;
				this.useCoupon = true;
				loading.dismiss();
			} else {
				console.log('kuponnya salah');
				this.couponItem = null;
				this.couponDisc = 0;
				loading.dismiss();
				this.showAlert('Kode Kupon Salah');
				this.useCoupon = false;
			}
		});
		} else {
			this.couponItem = null;
			console.log('kuponnya salah');
			this.couponDisc = 0;
			this.showAlert('Isi kode kupon');
			this.useCoupon = false;
		}
	}

}

