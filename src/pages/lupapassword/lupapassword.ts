import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-password',
  templateUrl: 'lupapassword.html'
})

export class forgotPasswordPage{
	
		
	constructor(
		public navCtrl: NavController,
  		private navParams: NavParams,
		public modalCtrl:ModalController,
		private loadingCtrl: LoadingController,	
	){}
	
	
	
	back(){
		
		this.navCtrl.push(ProfilePage);
	}	
}

