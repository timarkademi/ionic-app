import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController,LoadingController, AlertController } from 'ionic-angular';

import { Http, Headers, Response, RequestOptions, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';

import { User } from '../../models/user';
import { Course } from '../../models/course';

import { ConfigService } from '../../services/config';
import { CourseService } from '../../services/course';
import { UserService } from '../../services/users';
import { WishlistService } from '../../services/wishlist';

import { Storage } from '@ionic/storage';

import { SearchPage } from '../search/search';
import { CoursePage } from '../course/course';
import { ProfilePage } from '../profile/profile';
   
    
@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})

export class OrderPage implements OnInit{
	
	order : any;
	
	hours: number =24;
	minutes: number =0;
	seconds: number =0;
	
	counter:any;
	
	constructor(
		
        private config: ConfigService,
  		private navParams: NavParams,
		private userService: UserService,
		public modalCtrl:ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
				  		  
		public navCtrl: NavController,
  		) {}

	ngOnInit() {

		console.log('masuk menunggu pembayaran');
		this.order = this.navParams.data['order'];
		console.log('Sampe');
		console.log(this.order);

 	}
    openSearch(){
        let modal = this.modalCtrl.create(SearchPage);
        modal.present();
    }
    backToCourse(){
      //this.navCtrl.push(CoursePage,{'id':this.coursestatus.course_id});
    } 
	

    ionViewLoaded(){
        //this.runTimer();
    }
	
	ionViewDidLoad(){
		let distance = (24 * 60 * 60);
		let x = setInterval(() =>{
			distance = distance - 1;
			
			this.hours = Math.floor((distance % ( 1* 60 * 60 * 24)) / ( 1* 60 * 60));
			this.minutes = Math.floor((distance % ( 1* 60 * 60)) / ( 1* 60));
			this.seconds = Math.floor((distance % ( 1* 60)) / 1);
			//console.log('Time:'+this.hours+':'+this.minutes+':'+this.seconds);
		},1000);
			
	}
		
	showAlert(message:string=null,message2:string=null) {
		let alert = this.alertCtrl.create({
			title:message,
			subTitle:message2,
			buttons:['Ok']
		});
		alert.present();
	}
}

