import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Slides, AlertController } from 'ionic-angular';

//import {StatusBar} from 'ionic-native';
import { StatusBar } from '@ionic-native/status-bar';
import { Content } from 'ionic-angular';

import { ProfilePage } from '../profile/profile';
import { SearchPage } from '../search/search';

import { DirectoryPage } from '../directory/directory';
import { CoursePage } from '../course/course';

import { CourseService } from '../../services/course';
import { ConfigService } from '../../services/config';
import { WalletService } from '../../services/wallet';

import { WishlistService } from '../../services/wishlist';

import { UserService } from '../../services/users';

import { User } from '../../models/user';
import { Course } from '../../models/course';
import { CourseCategory } from '../../models/course';

import { FixedScrollHeader } from '../../components/fixed-scroll-header/fixed-scroll-header';
import { Coursecard } from '../../components/coursecard/coursecard';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Coursecard2 } from '../../components/coursecard2/coursecard2';
import { BlogService } from '../../services/blog';
import { BlogPage } from '../blog/blog';
import { QuizPage } from '../quiz/quiz';
import { QuizPage2 } from '../quiz2/quiz2';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit{
	
	isLoggedIn: boolean = false;
	user: User;
	featured: Course[] = [];
	popular: Course[] = [];
	categories: CourseCategory[] = [];
	categoryPage = DirectoryPage;
	course:Course;
	profilePage=ProfilePage;
	coursePage= CoursePage;
	blogPage=BlogPage;
	
	accord: boolean = false;
	accordSize: string;
	
	cobacategory: Course[] = [];
	testcategory: any[] = [];
	
	
	@ViewChild('Carousel') carousel: Slides;
	
	constructor(public navCtrl: NavController,
		private courseService: CourseService, 
		private modalCtrl: ModalController,
		public loadingController: LoadingController,
		public userService:UserService,
		private config:ConfigService,
		private wishlistService:WishlistService,
		private blogService: BlogService,
		private walletService:WalletService,
		private alertCtrl: AlertController,) {
		
	}

	ngOnInit() {
		console.log('waiting to be loaded');

		let loading = this.loadingController.create({
            content: '<img src="assets/images/bubbles.svg">',
            duration: 15000,//this.config.get_translation('loadingresults'),
            spinner:'hide',
            showBackdrop:true,

        });

        loading.present();



		
		
		this.config.isLoading().then(res=>{
			if(res){
				this.config.track = res;
			}

			this.courseService.getFeaturedCourses().subscribe(featured =>{
				if(featured){
					this.featured = featured;
				}
			});
			
			this.courseService.getPopularCourses().subscribe(popular =>{
				if(popular){
					this.popular = popular;
					
				}
				loading.dismiss();
			});

			this.courseService.getAllCourseCategory().subscribe(cats =>{
				if(cats){
					this.categories = cats;
				}
			});
			
			this.courseService.getTestCategoryCourses().subscribe(cobacategory =>{
				if(cobacategory){
					console.log('boy'+cobacategory);
					this.cobacategory = cobacategory;
					
				}
			});
			
			
			this.blogService.getPosts().subscribe(testcategory =>{
				if(testcategory){
					
					this.testcategory = testcategory;
				}
			});
		});
    	this.wishlistService.getWishList();
	}
	
	openSearch(){
		let modal = this.modalCtrl.create(SearchPage);
    	modal.present();
		// this.navCtrl.push(SearchPage);
	}
	
	toggleAccordion(){
		if(this.accord){
			this.accord = false;
			this.accordSize = '100px';
			console.log('klik');
		} else {
			this.accord = true;
			this.accordSize = '100%';
			console.log('click');
		}
	}
	
	getSize(){
		return this.accordSize;
	}
	
showPage(){
      this.navCtrl.push(CoursePage,{'id':6427});	
	}
	
showPage2(){
      this.navCtrl.push(CoursePage,{'id':2475});	
}
	
	
	
	
Quiz(){
	
	if(this.config.isLoggedIn){
	
      this.navCtrl.push(QuizPage);	
	  
	}else{

	 this.navCtrl.push(ProfilePage);

		
	}	
}

showQuiz2(){
	
	if(this.config.isLoggedIn){
	
      this.navCtrl.push(QuizPage2);	
	  
	}else{

	 this.navCtrl.push(ProfilePage);
	
	}	
}	

}